![HashTags Logo](Logo.jpeg "HashTags")

## About

HashTags is an application which is intended to act as a Search Engine for Social Media Platforms and will fetch data from various platforms and display the content on one page.

Initially, it will have the capacity to work with:
    - Instagram
    - Twitter
    - Reddit
    
## Team

HashTags is an open source project but at the moment following members are working on its development:
    - @sharmaamneet
    - @pagalprogrammer
    - @jotchandi
    - @deepali017
    
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
    
## License

This project is licensed under the [GNU GPL V3.0](https://choosealicense.com/licenses/gpl-3.0/) License. 